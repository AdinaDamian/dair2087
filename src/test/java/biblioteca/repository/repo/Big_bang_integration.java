package biblioteca.repository.repo;

import biblioteca.control.BibliotecaCtrl;
import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class Big_bang_integration {
    private CartiRepoInterface cartiRepoInterface=new CartiRepo();
    private BibliotecaCtrl bibliotecaCtrl=new BibliotecaCtrl(cartiRepoInterface);

    @Test
    public void TestA()throws Exception{
        Integer size=bibliotecaCtrl.getCarti().size();
        List<String>a=new ArrayList<String>();
        a.add("Ana");
        List<String>c=new ArrayList<String>();
        c.add("ana");
        Carte c1=new Carte("Blabla",a,"2020","editura",c);
        bibliotecaCtrl.adaugaCarte(c1);
        assertEquals(size+1,bibliotecaCtrl.getCarti().size());
    }
    @Test
    public void TestB()throws Exception{
        List<Carte> carti=bibliotecaCtrl.cautaCarteDupaAutor("Sadoveanu");
        assertEquals(carti.size(),1);
    }
    @Test
    public void TestC()throws Exception{
        List<Carte> carti=bibliotecaCtrl.getCartiOrdonateDinAnul("1973");
        assertTrue(carti.size()>0);
    }

    @Test
    public void TestPABC()throws Exception{
        Integer size=bibliotecaCtrl.getCarti().size();
        List<String>a=new ArrayList<String>();
        a.add("Emanuel");
        List<String>c=new ArrayList<String>();
        c.add("emanuel");
        Carte c1=new Carte("Bla",a,"2000","editura",c);
        bibliotecaCtrl.adaugaCarte(c1);
        assertEquals(size+1,bibliotecaCtrl.getCarti().size());
        List<Carte> carti=bibliotecaCtrl.cautaCarteDupaAutor("Emanuel");
        assertEquals(carti.size(),1);
        carti=bibliotecaCtrl.getCartiOrdonateDinAnul("2000");
        assertEquals(carti.size(),1);

    }
}