package biblioteca.util;

import biblioteca.model.Carte;

import java.util.ArrayList;
import java.util.List;

public class CarteValidator {

	public static boolean isAutorOk(List<String> s) {
		for(int st=0;st<s.size();st++) {
			String []t = s.get(st).split(" ");
			if (!t[st].matches("[a-zA-Z]+"))
				return false;
		}
		return true;
	}
	public static boolean isCuvantCOk(List<String> s) {
		for(int st=0;st<s.size();st++) {
			//String []t = s.get(st).split(" ");
			if (!s.get(st).matches("[a-zA-Z]+"))
				return false;
		}
		return true;
	}

 	public static boolean isOKString(String s){
		String []t = s.split(" ");
		for(int st=0;st<t.length;st++){
			if(!t[st].matches("[a-zA-Z]+"))
				return false;
		}
		return true;
//		if(t.length==2){
//			boolean ok1 = t[0].matches("[a-zA-Z]+");
//			boolean ok2 = t[1].matches("[a-zA-Z]+");
//			if(ok1==ok2 && ok1==true){
//				return true;
//			}
//			return false;
//		}
//		return s.matches("[a-zA-Z]+");
 		/*
		String []t = s.split(" ");
		for(int i=0;i<t.length;i++){
			boolean ok = t[i].matches("[a-zA-Z]+");
			if(ok==true){
				return true;
			}
			return false;
		}
		return s.matches("[a-zA-Z]+");
		*/
	}

	public static void validateAutor(String autor)throws Exception{
 		if(!isOKString(autor)){
			throw new Exception("Autor invalid!");
		}
	}

	public static void validateAn(String an)throws Exception{
 		if(!isNumber(an)){
			throw new Exception("An invalid!");
		}
	}


	public static void validateCarte(Carte c)throws Exception{
		if(Integer.parseInt(c.getAnAparitie())<=1500)
			throw new Exception("Anul trebuie sa fie mai mare de 1500!");
		if(c.getTitlu().length()>=40)
			throw new Exception("Titlul nu poate avea lungimea mai mare de 40!");

		if(c.getCuvinteCheie()==null){
			throw new Exception("Lista cuvinte cheie vida!");
		}
		if(c.getReferenti()==null){
			throw new Exception("Lista autori vida!");
		}
		if(!isOKString(c.getTitlu()))
			throw new Exception("Titlu invalid!");
		if(!isAutorOk(c.getReferenti())){
				throw new Exception("Autor invalid!");
		}
		for(String s:c.getCuvinteCheie()) {
			if (!s.matches("[a-zA-Z]+"))
				throw new Exception("Cuvant cheie invalid!");
		}
		if(!CarteValidator.isNumber(c.getAnAparitie()))
			throw new Exception("Editura invalid!");
	}
	
	public static boolean isNumber(String s){
		return s.matches("[0-9]+");
	}

}
